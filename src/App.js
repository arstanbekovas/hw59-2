import React, { Component } from 'react';
import './App.css';
import Jokes from "./container/Jokes/Jokes";

class App extends Component {
  render() {
    return (
        <Jokes />
    );
  }
}

export default App;
