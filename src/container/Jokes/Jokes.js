import React, {Component} from 'react';
import Button from "../../components/UI/Button/Button";

class Jokes extends Component {

    state = {
        jokes: []
    };


    showJoke = () => {
        const P_URL = 'https://api.chucknorris.io/jokes/random';
        const jokes = [];
        for(let i = 0; i < 5; i++) {
            jokes.push(
                fetch(P_URL).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw new Error('Something went wrong with network request');
                })
            )
        }
        Promise.all(jokes).then(jokes => {
            console.log(jokes.map(joke => joke.value));

            this.setState({jokes: jokes.map(joke => joke.value + ' ---->  ')});
        }).catch(error => {
            console.log(error);
        });
    };

    componentDidMount() {
        this.showJoke()
    }

    render() {
        return (
            <div>
                {this.state.jokes}
                <Button click={this.showJoke}/>
            </div>
        )
    }
}
export default Jokes;