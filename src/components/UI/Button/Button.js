import React, {Component} from 'react';
import './Button.css'

class Button extends Component {

    shouldComponentUpdate () {
        return false;
    }

    render () {
        return (
            <button className="New" onClick={this.props.click}>New Joke</button>
        )
    }

}

export default Button;